package com.devcamp.jbr430.jbr430;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr430Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr430Application.class, args);
	}

}
