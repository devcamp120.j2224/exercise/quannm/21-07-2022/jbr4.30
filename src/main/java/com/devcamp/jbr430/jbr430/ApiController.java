package com.devcamp.jbr430.jbr430;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiController {
    @CrossOrigin
    @GetMapping("/books")
    public ArrayList<Book> getListBook() {
        ArrayList<Book> listBook = new ArrayList<Book>();

        ArrayList<Author> listAuthor1 = new ArrayList<Author>();
        ArrayList<Author> listAuthor2 = new ArrayList<Author>();
        ArrayList<Author> listAuthor3 = new ArrayList<Author>();

        Author author1 = new Author("AAA", "aaa@gmail.com", 'F');
        Author author2 = new Author("BBB", "bbb@gmail.com", 'M');
        Author author3 = new Author("CCC", "ccc@gmail.com", 'M');
        Author author4 = new Author("DDD", "ddd@gmail.com", 'F');
        Author author5 = new Author("EEE", "eee@gmail.com", 'M');
        Author author6 = new Author("FFF", "fff@gmail.com", 'F');

        listAuthor1.add(author1);
        listAuthor1.add(author2);
        listAuthor2.add(author3);
        listAuthor2.add(author4);
        listAuthor3.add(author5);
        listAuthor3.add(author6);

        Book book1 = new Book("Math", listAuthor1, 500);
        Book book2 = new Book("JavaScript", listAuthor2, 400);
        Book book3 = new Book("Java", listAuthor3, 600);

        listBook.add(book1);
        listBook.add(book2);
        listBook.add(book3);

        return listBook;
    }

    // public static void main(String[] args) {
    //     ArrayList<Author> listAuthor1 = new ArrayList<Author>();
    //     ArrayList<Author> listAuthor2 = new ArrayList<Author>();
    //     ArrayList<Author> listAuthor3 = new ArrayList<Author>();

    //     Author author1 = new Author("AAA", "aaa@gmail.com", 'F');
    //     Author author2 = new Author("BBB", "bbb@gmail.com", 'M');
    //     Author author3 = new Author("CCC", "ccc@gmail.com", 'M');
    //     Author author4 = new Author("DDD", "ddd@gmail.com", 'F');
    //     Author author5 = new Author("EEE", "eee@gmail.com", 'M');
    //     Author author6 = new Author("FFF", "fff@gmail.com", 'F');

    //     listAuthor1.add(author1);
    //     listAuthor1.add(author2);
    //     listAuthor2.add(author3);
    //     listAuthor2.add(author4);
    //     listAuthor3.add(author5);
    //     listAuthor3.add(author6);

    //     Book book1 = new Book("Math", listAuthor1, 500);
    //     Book book2 = new Book("JavaScript", listAuthor2, 400);
    //     Book book3 = new Book("Java", listAuthor3, 600);
    //     System.out.println(book1 + "," + book2 + "," + book3);
    // }
}
